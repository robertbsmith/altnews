// TODO(DEVELOPER): Change the values below using values from the initialization snippet: Firebase Console > Overview > Add Firebase to your web app.
// Initialize Firebase
var config = {
  apiKey: "AIzaSyA9fk6Ck8LOkknEAMq23BEeNmT1clpv5JA",
  databaseURL: "https://altnewsextension.firebaseio.com",
  storageBucket: "altnewsextension.appspot.com"
};
firebase.initializeApp(config);

/**
 * initApp handles setting up the Firebase context and registering
 * callbacks for the auth status.
 *
 * The core initialization is in firebase.App - this is the glue class
 * which stores configuration. We provide an app name here to allow
 * distinguishing multiple app instances.
 *
 * This method also registers a listener with firebase.auth().onAuthStateChanged.
 * This listener is called when the user is signed in or out, and that
 * is where we update the UI.
 *
 * When signed in, we also authenticate to the Firebase Realtime Database.
 */
function initApp() {
  // Listen for auth state changes.
  firebase.auth().onAuthStateChanged(function(user) {
    console.log('User state change detected from the Background script of the Chrome Extension:', user);
  });

  chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {

    console.log("dsls");
    if (changeInfo.status == 'complete' && tab.active) {

        chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT},
        function(tabs){
            var stringurl = encodeURIComponent(tabs[0].url).replace(/\./g, '%2E');
            checkIfURLExists(stringurl);

            console.log("it works!"); 
        }
      );
    }
})
}

window.onload = function() {
  initApp();
};


function checkIfURLExists(tab_url) {

  var ref = firebase.database().ref('url/' + tab_url);
  ref.once("value")
    .then(function(snapshot) {
      var name = snapshot.child("url").val();

      if(name == null)
      {
        console.log(tab_url);
        console.log("Doesn't exist");
      }
      else
      {  
          chrome.tabs.executeScript({
              code: 'document.body.style.backgroundColor="red"'
          });
      }
  });
}


